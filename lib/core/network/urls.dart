const baseUrl = 'https://fakestoreapi.com';
const loginUrl = '$baseUrl/auth/login';
const productsUrl = '$baseUrl/products';
String productUrl(int id) => '$productsUrl/$id';
