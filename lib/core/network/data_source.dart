import 'dart:convert';

import 'package:nano/core/error/excpetions.dart';

Future<T> dataSource<T>(Function call,
    {Function? model, bool isAuth = false}) async {
  final response = await call();
  print("responseIs: ${response.body}");
  var jsonResponse = json.decode(response.body);
  if (response.statusCode == 200) {
    if (isAuth) {
      return jsonResponse["token"];
    }
    if (model == null) {
      return Future.value(null);
    }
    return model(response.body);
  } else {
    throw HttpException(jsonResponse["message"]);
  }
}
