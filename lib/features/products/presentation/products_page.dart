import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/app/widgets/error_occured_widget.dart';
import 'package:nano/app/widgets/shimmer.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/presentation/cubit/products_cubit.dart';
import 'package:nano/features/products/presentation/widgets/product_list_widget.dart';

class ProductsPage extends StatelessWidget {
  const ProductsPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.background,
        appBar: PreferredSize(
            preferredSize: Size(MediaQuery.of(context).size.width, 80),
            child: Container(
              padding: const EdgeInsets.only(top: 30),
              decoration: const BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(107, 127, 153, 0.25),
                      blurRadius: 10,
                      offset: Offset(0, 4),
                    ),
                  ],
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(35),
                      bottomRight: Radius.circular(35))),
              child: const Center(
                child: Text(
                  "All Products",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 28,
                      fontWeight: FontWeight.w700),
                ),
              ),
            )),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 18, vertical: 26),
          child: BlocConsumer<ProductsCubit, ProductsState>(
              listener: (context, state) {},
              builder: (context, state) {
                if (state is LoadingState) {
                  return shimmer(ShimmerWidget());
                }
                if (state is ErrorState) {
                  return Center(
                    child: ErrorOccuredTextWidget(
                      fun: () =>
                          BlocProvider.of<ProductsCubit>(context).getProducts(),
                    ),
                  );
                }
                if (state is GotRequests) {
                  List<ProductModel> products = state.products;
                  return RefreshIndicator(
                    onRefresh: () =>
                        BlocProvider.of<ProductsCubit>(context).getProducts(),
                    child: ListView.separated(
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          // final pro = ProductModel(
                          //     id: 1,
                          //     title:
                          //         "Fjallraven - Foldsack No. 1 Backpack, Fits 15 Laptops",
                          //     category: Category.MEN_S_CLOTHING,
                          //     description:
                          //         "Your perfect pack for everyday use and walks in the forest. Stash your laptop (up to 15 inches) in the padded sleeve, your everyday",
                          //     image:
                          //         "https://fakestoreapi.com/img/81fPKd-2AYL._AC_SL1500_.jpg",
                          //     price: 109.95,
                          //     rating: Rating(count: 120, rate: 3.9));
                          return ProductListWidget(products[index]);
                        },
                        separatorBuilder: (context, index) =>
                            const SizedBox(height: 20),
                        itemCount: products.length),
                  );
                }
                return Center(
                  child: ErrorOccuredTextWidget(
                    fun: () =>
                        BlocProvider.of<ProductsCubit>(context).getProducts(),
                  ),
                );
              }),
        ));
  }
}
