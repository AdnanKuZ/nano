import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/features/products/data/product_model.dart';

class CustomRatingWidget extends StatelessWidget {
  const CustomRatingWidget(this.product, {super.key, this.secondaryColor});
  final ProductModel product;
  final Color? secondaryColor;
  @override
  Widget build(BuildContext context) {
    return Tooltip(
      message: product.rating.rate.toString(),
      child: RatingBar.builder(
        initialRating: product.rating.rate,
        minRating: 1,
        direction: Axis.horizontal,
        allowHalfRating: true,
        itemCount: 5,
        itemSize: 28,
        unratedColor: secondaryColor ?? AppColors.background,
        itemPadding: const EdgeInsets.symmetric(horizontal: 0),
        itemBuilder: (context, _) => const Icon(
          Icons.star,
          color: Color(0xFFFFE072),
        ),
        onRatingUpdate: (rating) {
          print(rating);
        },
      ),
    );
  }
}
