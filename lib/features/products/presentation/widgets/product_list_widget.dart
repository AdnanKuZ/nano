import 'package:flutter/material.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/presentation/product_page.dart';
import 'package:nano/features/products/presentation/widgets/rating_widget.dart';

class ProductListWidget extends StatelessWidget {
  const ProductListWidget(this.product, {super.key});
  final ProductModel product;
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(context,
          MaterialPageRoute(builder: (context) => ProductPage(product))),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            width: double.infinity,
            height: 220,
            alignment: Alignment.bottomLeft,
            padding: const EdgeInsets.fromLTRB(11, 0, 20, 10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                image: DecorationImage(
                    image: NetworkImage(product.image), fit: BoxFit.cover)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "${product.price} AED",
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w700),
                ),
                SizedBox(child: CustomRatingWidget(product))
              ],
            ),
          ),
          const SizedBox(height: 19),
          Text(product.title,
              style: const TextStyle(
                  color: Color(0xFF444B51),
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w300)),
          const SizedBox(height: 9),
          Text(product.description,
              style: const TextStyle(color: Color(0xFF08293B), fontSize: 13)),
          const SizedBox(height: 29),
          const Divider(
            thickness: 1.4,
            color: Color(0xFFD8D8D8),
          )
        ],
      ),
    );
  }
}
