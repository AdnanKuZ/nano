import 'package:flutter/material.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/app/widgets/button.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/presentation/widgets/rating_widget.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class SlideUpSection extends StatefulWidget {
  const SlideUpSection(this.product, {super.key});
  final ProductModel product;
  @override
  State<SlideUpSection> createState() => _SlideUpSectionState();
}

class _SlideUpSectionState extends State<SlideUpSection> {
  double turns = 1.0;
  @override
  Widget build(BuildContext context) {
    return SlidingUpPanel(
      panelSnapping: false,
      slideDirection: SlideDirection.UP,
      minHeight: MediaQuery.of(context).size.height * 0.3,
      maxHeight: MediaQuery.of(context).size.height * 0.5,
      borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(32), topRight: Radius.circular(32)),
      onPanelSlide: (position) {
        if (position > 0.4) {
          if (turns != 0.5) {
            setState(() {
              turns = 0.5;
            });
          }
        } else {
          if (turns != 1.0) {
            setState(() {
              turns = 1.0;
            });
          }
        }
      },
      panel: Column(
        children: [
          const SizedBox(height: 9),
          AnimatedRotation(
            turns: turns,
            duration: const Duration(milliseconds: 250),
            child:
                SizedBox(width: 16, child: Image.asset("assets/up_arrow.png")),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(14, 14, 25, 4),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Wrap(
                  alignment: WrapAlignment.spaceBetween,
                  // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 56,
                      height: 56,
                      padding: const EdgeInsets.all(15),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          boxShadow: const [
                            BoxShadow(
                              color: Color.fromRGBO(107, 127, 153, 0.25),
                              blurRadius: 10,
                              offset: Offset(0, 4),
                            ),
                          ],
                          borderRadius: BorderRadius.circular(25)),
                      child: Image.asset("assets/download.PNG"),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.09,
                    ),
                    const CustomElevatedButton(
                      title: "Order Now",
                      buttonColor: AppColors.mainLightBlue,
                      borderRadius: 62,
                      verticalPadding: 17,
                      horizantalPadding: 65,
                    ),
                  ],
                ),
                const SizedBox(height: 19),
                const Text("Description",
                    style: TextStyle(
                        color: Color(0xFF444B51),
                        fontStyle: FontStyle.italic,
                        fontWeight: FontWeight.w300)),
                const SizedBox(height: 9),
                Text(
                  widget.product.description,
                  style:
                      const TextStyle(color: Color(0xFF08293B), fontSize: 13),
                ),
                const SizedBox(height: 19),
                Container(
                  width: double.infinity,
                  padding: const EdgeInsets.all(8),
                  decoration: BoxDecoration(
                      color: const Color(0xFFF1F1F1),
                      borderRadius: BorderRadius.circular(12)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text(
                        "Reviews(100)",
                        style: TextStyle(
                            color: Color(0xFF444B51),
                            fontSize: 16,
                            fontWeight: FontWeight.w600),
                      ),
                      const SizedBox(height: 19),
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(children: [
                          Text(
                            widget.product.rating.rate.toString(),
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 32,
                                fontWeight: FontWeight.w600),
                          ),
                          const SizedBox(width: 35),
                          CustomRatingWidget(widget.product,secondaryColor: Colors.grey),
                        ]),
                      ),
                      const SizedBox(height: 10),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
