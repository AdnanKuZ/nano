import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano/core/funcs.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/data/product_repositories.dart';
part 'products_state.dart';

class ProductsCubit extends Cubit<ProductsState> {
  final ProductsRepositories _productsRepositories;
  ProductsCubit(this._productsRepositories) : super(GetProductsInitial());

  Future<void> getProducts() async {
    emit(LoadingState());
    final either = await _productsRepositories.getProducts();
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      emit(GotRequests(data));
    });
  }
}
