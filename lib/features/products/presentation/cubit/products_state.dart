part of 'products_cubit.dart';

abstract class ProductsState {
  const ProductsState();
}

class GetProductsInitial extends ProductsState {}

class LoadingState extends ProductsState {}

class GotRequests extends ProductsState {
  final List<ProductModel> products;

  GotRequests(this.products);
}

class ErrorState extends ProductsState {
  final String message;

  ErrorState(this.message);
}
