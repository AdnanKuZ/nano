import 'package:flutter/material.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/presentation/widgets/slide_up_section.dart';

class ProductPage extends StatelessWidget {
  const ProductPage(this.product, {super.key});
  final ProductModel product;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          SizedBox(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height * 0.75,
              child: Image.network(
                product.image,
                fit: BoxFit.cover,
              )),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 20, right: 9, left: 9),
                child: Row(
                  children: [
                    InkWell(
                      onTap: () => Navigator.pop(context),
                      child: Material(
                        elevation: 2,
                        borderRadius: BorderRadius.circular(12),
                        child: Container(
                          width: 40,
                          height: 40,
                          padding: const EdgeInsets.all(11),
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(12)),
                          child: Center(
                              child: Image.asset("assets/arrow_back.png")),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 19),
              Padding(
                padding: const EdgeInsets.only(left: 9),
                child: Text(
                  product.title,
                  style: const TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                      fontWeight: FontWeight.w700),
                ),
              ),
            ],
          ),
          SlideUpSection(product)
        ],
      ),
    ));
  }
}
