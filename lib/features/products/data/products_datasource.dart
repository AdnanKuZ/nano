import 'package:http/http.dart';
import 'package:nano/core/network/data_source.dart';
import 'package:nano/core/network/urls.dart';
import 'package:nano/features/products/data/product_model.dart';

class ProductsDataSource {
  final Client _client;
  ProductsDataSource(this._client);
  Future<List<ProductModel>> getProducts() async => dataSource(
      () => _client.get(
            Uri.parse(productsUrl),
            headers: {
              'Content-Type': 'application/json',
            },
          ),
      model: productModelFromJson);
}
