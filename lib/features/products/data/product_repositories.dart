import 'package:dartz/dartz.dart';
import 'package:nano/core/error/failures.dart';
import 'package:nano/core/network/repositories.dart';
import 'package:nano/features/products/data/product_model.dart';
import 'package:nano/features/products/data/products_datasource.dart';

class ProductsRepositories {
  final ProductsDataSource _productsDataSource;
  ProductsRepositories(this._productsDataSource);
  Future<Either<Failure, List<ProductModel>>> getProducts() async =>
      await repository(
        () async => await _productsDataSource.getProducts(),
      );
}
