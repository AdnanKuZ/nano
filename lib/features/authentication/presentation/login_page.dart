import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/app/extensions/snack_bar_build_context.dart';
import 'package:nano/app/page_transition.dart';
import 'package:nano/features/authentication/presentation/cubit/auth_cubit.dart';
import 'package:nano/features/authentication/presentation/widgets/email_text_field.dart';
import 'package:nano/features/authentication/presentation/widgets/pass_text_field.dart';
import 'package:nano/features/products/presentation/cubit/products_cubit.dart'
    as productsCubit;
import 'package:nano/features/products/presentation/products_page.dart';
import 'package:progress_state_button/iconed_button.dart';
import 'package:progress_state_button/progress_button.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailCon = TextEditingController();
  final TextEditingController _passCon = TextEditingController();
  ButtonState _buttonState = ButtonState.idle;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: MediaQuery.of(context).size.height * 0.46,
              child: Stack(
                children: [
                  SizedBox(
                      width: MediaQuery.of(context).size.width,
                      child: Image.asset(
                        "assets/splash_background.png",
                        fit: BoxFit.cover,
                      )),
                  Center(
                    child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: Image.asset("assets/logo.png")),
                  ),
                  const Align(
                    alignment: Alignment.bottomLeft,
                    child: Padding(
                      padding: EdgeInsets.only(left: 36, bottom: 30),
                      child: Text(
                        "Log In",
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 28,
                            fontWeight: FontWeight.w700),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 36, vertical: 54),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text("Email", style: TextStyle(color: Colors.black)),
                    EmailTextField(
                      emailCon: _emailCon,
                    ),
                    const SizedBox(height: 38),
                    const Text("Password",
                        style: TextStyle(color: Colors.black)),
                    PassTextField(
                      passCon: _passCon,
                    ),
                    const SizedBox(height: 24),
                    Row(
                      children: [
                        BlocConsumer<AuthCubit, AuthState>(
                            listener: (context, state) async {
                          if (state is AuthenticatedState) {
                            context.showSuccessSnackBar('Login Successful');
                            setState(() {
                              _buttonState = ButtonState.success;
                            });
                            await Future.delayed(const Duration(seconds: 1));
                            BlocProvider.of<productsCubit.ProductsCubit>(
                                    context)
                                .getProducts();
                            Navigator.pushReplacement(context,
                                PageTransition(widget: const ProductsPage()));
                          } else if (state is ErrorState) {
                            setState(() {
                              _buttonState = ButtonState.fail;
                            });
                            Fluttertoast.showToast(msg: state.message);
                          }
                        }, builder: (context, state) {
                          return Expanded(
                            child: ProgressButton.icon(
                                height: 74.0,
                                minWidthStates: const [
                                  ButtonState.fail,
                                  ButtonState.idle,
                                  ButtonState.success
                                ],
                                iconedButtons: {
                                  ButtonState.idle: const IconedButton(
                                      text: 'Continue',
                                      icon: Icon(
                                        Icons.abc,
                                        size: 0,
                                      ),
                                      color: AppColors.mainLightBlue),
                                  ButtonState.loading: IconedButton(
                                      text: 'Loading',
                                      color: Colors.deepPurple.shade700),
                                  ButtonState.fail: IconedButton(
                                      text: 'Failed',
                                      icon: const Icon(Icons.cancel,
                                          color: Colors.white),
                                      color: Colors.red.shade300),
                                  ButtonState.success: IconedButton(
                                      text: 'Success',
                                      icon: const Icon(
                                        Icons.check_circle,
                                        color: Colors.white,
                                      ),
                                      color: Colors.green.shade400)
                                },
                                textStyle: const TextStyle(
                                    color: Colors.white, fontSize: 17),
                                onPressed: onPressed,
                                state: _buttonState),
                          );
                        }),
                      ],
                    ),
                    const SizedBox(height: 43),
                    const Center(
                      child: Text("NEED HELP?",
                          style: TextStyle(color: Colors.black)),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void onPressed() {
    Map<String, dynamic> data = {
      "username": _emailCon.text.trim(),
      "password": _passCon.text.trim()
    };
    if (_formKey.currentState!.validate()) {
      setState(() {
        _buttonState = ButtonState.loading;
      });
      BlocProvider.of<AuthCubit>(context).login(data);
    }
  }
}
