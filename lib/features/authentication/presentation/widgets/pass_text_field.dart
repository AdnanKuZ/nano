import 'package:flutter/material.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/app/widgets/text_form_field.dart';
import 'package:nano/features/authentication/presentation/widgets/show_pass_widget.dart';

class PassTextField extends StatefulWidget {
  const PassTextField({super.key, required this.passCon});
  final TextEditingController passCon;

  @override
  State<PassTextField> createState() => _PassTextFieldState();
}

class _PassTextFieldState extends State<PassTextField> {
  bool isHidden = false;

  @override
  Widget build(BuildContext context) {
    return CustomTextFormField(
      controller: widget.passCon,
      fillColor: AppColors.background,
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 7),
      textColor: AppColors.textColor,
      suffix: ShowPassWidget(
        showHide: () {
          setState(() {
            isHidden = !isHidden;
          });
        },
        isHidden: isHidden,
      ),
      isObscur: isHidden,
      validator: (value) {
        if (value!.length < 6) {
          return "Password should not be less than 7 characters long";
        }
        return null;
      },
    );
  }
}
