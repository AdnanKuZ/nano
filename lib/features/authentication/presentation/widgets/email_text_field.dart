import 'package:flutter/material.dart';
import 'package:nano/app/constants/colors.dart';
import 'package:nano/app/widgets/text_form_field.dart';

class EmailTextField extends StatefulWidget {
  const EmailTextField({super.key, required this.emailCon});
  final TextEditingController emailCon;

  @override
  State<EmailTextField> createState() => _EmailTextFieldState();
}

class _EmailTextFieldState extends State<EmailTextField> {
  bool _isValid = false;
  @override
  Widget build(BuildContext context) {
    return CustomTextFormField(
      controller: widget.emailCon,
      fillColor: AppColors.background,
      textDirection: TextDirection.ltr,
      padding: const EdgeInsets.symmetric(vertical: 18, horizontal: 7),
      textColor: AppColors.textColor,
      suffix: Icon(
        Icons.check_circle_outline_outlined,
        color: _isValid?AppColors.mainLightBlue:Colors.grey,
        size: 18,
      ),
      validator: (value) {
        // if (!RegExp(
        //         r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        //     .hasMatch(value!))
        if(value!.isEmpty)
             {
          // return 'Please enter a valid email address';
          return 'Please enter a username or email';
        }
        return null;
      },
      onChanged: (value) {
        setState(() {
          _isValid = RegExp(
                  r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
              .hasMatch(value);
        });
      },
    );
  }
}
