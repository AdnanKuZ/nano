part of 'auth_cubit.dart';

abstract class AuthState {
  const AuthState();
}

class AuthInitial extends AuthState {}

class LoadingState extends AuthState {}

class AuthenticatedState extends AuthState {
  final String token;

  AuthenticatedState(this.token);
}
class ErrorState extends AuthState {
  final String message;

  ErrorState(this.message);
}
