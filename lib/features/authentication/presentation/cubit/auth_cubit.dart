import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano/app/di.dart';
import 'package:nano/core/funcs.dart';
import 'package:nano/features/authentication/data/auth_repositories.dart';

part 'auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  final AuthRepositories _authRepositories;
  AuthCubit(this._authRepositories) : super(AuthInitial());
  Future<void> login(Map<String, dynamic> data) async {
    print(data);
    emit(LoadingState());
    final either = await _authRepositories.login(data);
    either.fold((error) async {
      final errorMessage = getErrorMessage(error);
      emit(ErrorState(errorMessage));
    }, (data) {
      DI.userService.setUserToken(data);
      emit(AuthenticatedState(data));
    });
  }

}
