
import 'package:shared_preferences/shared_preferences.dart';

class UserService {
  final SharedPreferences _preferences;
  UserService(this._preferences);
  static const String user_token_key = "user-token";

  Future setUserToken(String token) async =>
      _preferences.setString(user_token_key, token);

  String? getUserToken() => _preferences.getString(user_token_key);
}
