import 'package:dartz/dartz.dart';
import 'package:nano/core/error/failures.dart';
import 'package:nano/core/network/repositories.dart';
import 'package:nano/features/authentication/data/auth_datasource.dart';

class AuthRepositories {
  final AuthDataSource _authDataSource;
  AuthRepositories(this._authDataSource);

  Future<Either<Failure, String>> login(Map<String, dynamic> data) async =>
      await repository(() async => await _authDataSource.login(data));
}
