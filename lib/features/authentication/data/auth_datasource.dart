import 'dart:convert';

import 'package:http/http.dart';
import 'package:nano/core/network/data_source.dart';
import 'package:nano/core/network/urls.dart';

class AuthDataSource {
  final Client _client;
  AuthDataSource(this._client);
  Future<String> login(Map<String, dynamic> data) async {
    return dataSource(
        () => _client.post(
              Uri.parse(loginUrl),
              body: json.encode(data),
              headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            ),
        isAuth: true);
  }
}
