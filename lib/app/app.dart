import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:nano/app/custom_splash_screen.dart';
import 'package:nano/app/di.dart';
import 'package:flutter_native_splash/flutter_native_splash.dart';
import 'package:nano/features/products/presentation/products_page.dart';

class NanoApp extends StatelessWidget {
  static Future<void> init() async {
    //splash screen
    FlutterNativeSplash.preserve(
        widgetsBinding: WidgetsFlutterBinding.ensureInitialized());
    await DI.init();
    await SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    Future.delayed(
      const Duration(milliseconds: 50),
      () => FlutterNativeSplash.remove(),
    );
  }

  const NanoApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => DI.authCubitFactory()),
        BlocProvider(create: (_) => DI.productsCubitFactory()),
      ],
      child: MaterialApp(
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const MyCustomSplashScreen()),
    );
  }
}
