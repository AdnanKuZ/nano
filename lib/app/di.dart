import 'package:http/http.dart';
import 'package:get_it/get_it.dart';
import 'package:nano/features/authentication/data/auth_datasource.dart';
import 'package:nano/features/authentication/data/auth_repositories.dart';
import 'package:nano/features/authentication/presentation/cubit/auth_cubit.dart';
import 'package:nano/features/authentication/services/user_service.dart';
import 'package:nano/features/products/data/product_repositories.dart';
import 'package:nano/features/products/data/products_datasource.dart';
import 'package:nano/features/products/presentation/cubit/products_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class DI {
  static GetIt get di => GetIt.instance;

  static Future<void> init() async {
    final preferences = await SharedPreferences.getInstance();
    di.registerLazySingleton<UserService>(() => UserService(preferences));

    di.registerLazySingleton<Client>(() => Client());
    registerAuth();
    registerProducts();
  }

  static void registerAuth() async {
    di.registerLazySingleton<AuthDataSource>(
        () => AuthDataSource(di<Client>()));
    di.registerLazySingleton<AuthRepositories>(
        () => AuthRepositories(di<AuthDataSource>()));
    di.registerFactory<AuthCubit>(() => AuthCubit(di<AuthRepositories>()));
  }
 static void registerProducts() async {
    di.registerLazySingleton<ProductsDataSource>(
        () => ProductsDataSource(di<Client>()));
    di.registerLazySingleton<ProductsRepositories>(
        () => ProductsRepositories(di<ProductsDataSource>()));
    di.registerFactory<ProductsCubit>(() => ProductsCubit(di<ProductsRepositories>()));
  }
  static UserService get userService => di.get<UserService>();
  static AuthCubit authCubitFactory() => di.get<AuthCubit>();
  static ProductsCubit productsCubitFactory() => di.get<ProductsCubit>();
}
