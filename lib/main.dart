import 'package:flutter/material.dart';
import 'package:nano/app/app.dart';

void main() async {
  await NanoApp.init();
  runApp(const NanoApp());
}
